vsync = true
fullscreen = false -- Not yet implemented ...

--[[
					DEBUG CONFIGURATION OPTIONS

	If you need to supply errors or do run time analysis of a problem.
	Please read this comment carefully...

	debugVerboseLevelFile    <<<< The minimum severity of the message to write to log.txt.
	debugVerboseLevelConsole <<<< The minimum severity of the message to write to console window.
	showConsole              <<<< Display the console window.

	Message severity codes:

	1 = Debug messages. (Mainly for devs.)
	2 = Normal messages. (Something important needs to be logged to the screen that is NOT an error/warning.)
	3 = Warning messages (When something went wrong that's not game stopping.)
	4 = Error messages (When something critical happened. i.e asset failed to load, Lua syntax error.)
--]]

loggingVerboseLevelFile    = 1
loggingVerboseLevelConsole = 1
showConsole                = false -- Not yet implemented ...
developerMode              = false