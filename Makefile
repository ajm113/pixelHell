#OBJS specifies which files to compile as part of the project

OBJS = $(wildcard ./neo/engine/*.cpp)  $(wildcard ./neo/engine/**/*.cpp) $(wildcard ./neo/engine/gamesys/assetLoader/*.cpp) $(wildcard ./neo/engine/gamesys/assetLoader/**/*.cpp)

#CC specifies which compiler we're using
CC = g++

#INCLUDE_PATHS specifies the additional include paths we'll need
INCLUDE_PATHS = -IC:\\mingw_dev_lib\\SDL2-2.0.4\\include -IC:\\mingw_dev_lib\\lua\\include

#LIBRARY_PATHS specifies the additional library paths we'll need
LIBRARY_PATHS = -LC:\mingw_dev_lib\\SDL2-2.0.4\lib -LC:\mingw_dev_lib\\lua

#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
# -Wl,-subsystem,windows gets rid of the console window
COMPILER_FLAGS = -Wall -Wl,-subsystem,console

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS = -lmingw32  -lSDL2main -lSDL2 -lopengl32 -lglu32 -llua53

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = pixelHell

#This is the target that compiles our executable
all : pixelHellEngine

pixelHellEngine: $(OBJS)
	$(CC) $(OBJS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

clean:
	del -f $(OBJ_NAME).exe log.txt