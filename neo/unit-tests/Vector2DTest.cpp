#include "QUnit/QUnit.hpp"
#include <iostream>

#include "../engine/gamesys/math/Vector2D.hpp"

class QUnitDemo {
public:

    QUnit::UnitTest qunit;

	void testBasicArithmetic() {

        phVector2D x(1,1);
        phVector2D y(1,1);

        // Multiplacation
        QUNIT_IS_EQUAL((phVector2D(1,1) == (x * y)), true);

        // Division
        QUNIT_IS_EQUAL((phVector2D(1,1) == (x / 1.0f)), true);

        // Add
        QUNIT_IS_EQUAL((phVector2D(2,2) == (x + y)), true);

        // Subtract
        QUNIT_IS_EQUAL((phVector2D(0,0) == (x - y)), true);
	}

    void testHelperFunctions() {

        phVector2D x(1,1);
        phVector2D y(1,1);

        // Dot
        QUNIT_IS_EQUAL(x.dot(y), 2.0f);

        // Magnitudes
        QUNIT_IS_EQUAL(x.magnitude(), 1.41421356237f);
        QUNIT_IS_EQUAL(x.inverseMagnitude(), 0.70710678118809504880169211101327f);
        QUNIT_IS_EQUAL(x.squareMagnitude(), 2.0f);
    }

public:

    QUnitDemo(std::ostream & out, int verboseLevel = QUnit::verbose)
        : qunit(out, verboseLevel) {}

    int run() {
        testBasicArithmetic();
        testHelperFunctions();
        return qunit.errors();
    }
        
};
    
int main() {
    return QUnitDemo(std::cerr).run();
}