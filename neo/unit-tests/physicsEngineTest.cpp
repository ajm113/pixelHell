#include "QUnit/QUnit.hpp"
#include <iostream>

#include "../engine/physics/Physics.hpp"

class QUnitDemo {
public:

    QUnit::UnitTest qunit;

	void testSpheres() {

		/* Dummy player.. */
        phPhysicsObject* object1 = new phPhysicsObject();
        object1->setPosition(phVector2D(2,0));
        object1->setSize(phVector2D(2,1));
        object1->setVelocity(phVector2D(0,0));
        object1->setMass(1.0);
        object1->setCollisionModel(phPhysicsObject::MODEL_SPHERE);

        /* Create obstical.. */
        phPhysicsObject* object2 = new phPhysicsObject();
        object2->setPosition(phVector2D(3,0));
        object2->setSize(phVector2D(1,3));
        object2->setMass(0);
        object2->setCollisionModel(phPhysicsObject::MODEL_SPHERE);

        phVector2D normal;
        QUNIT_IS_EQUAL(object1->collisionTest(object2, normal), true);

        object2->setPosition(phVector2D(-10,0));
        QUNIT_IS_EQUAL(object1->collisionTest(object2, normal), false);


        delete object1;
        delete object2;
	}

public:

    QUnitDemo(std::ostream & out, int verboseLevel = QUnit::verbose)
        : qunit(out, verboseLevel) {}

    int run() {
        testSpheres();
        return qunit.errors();
    }
        
};
    
int main() {
    return QUnitDemo(std::cerr).run();
}