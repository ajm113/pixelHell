#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <GL\GLU.h>

#include "../gamesys/Logger.hpp"
#include "../gamesys/math/Vector2D.hpp"

class phRenderer
{
public:
	phRenderer(phLogger* logger) { m_logger = logger; }
	~phRenderer() {}

	static void clearColorBuffer();

	virtual bool render() = 0;

	bool checkGLErrors();

protected:

	void enableTextures(unsigned int uid, bool transparent = true);
	void disableTextures();

	void drawBoundingBox(phVector2D position, phVector2D size);
	void drawBoundingSphere(phVector2D pos, float radius);
	void drawBoundingBoxAABB(phVector2D min, phVector2D max);
	void drawSprite(phVector2D position, phVector2D size);

private:
	phLogger* m_logger;
};


#endif