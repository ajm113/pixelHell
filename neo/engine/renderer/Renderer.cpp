#include "Renderer.hpp"

void phRenderer::clearColorBuffer()
{
	glClear( GL_COLOR_BUFFER_BIT );

	//Reset modelview matrix
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
}

bool phRenderer::checkGLErrors()
{
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		m_logger->printWarning( "OpenGL Error: %s", gluErrorString( error ));
		return true;
	}

	return false;
}

void phRenderer::enableTextures(unsigned int uid, bool transparent)
{
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);

	if(transparent)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindTexture(GL_TEXTURE_2D, uid);  
}

void phRenderer::disableTextures()
{
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

void phRenderer::drawSprite(phVector2D position, phVector2D size)
{
	glPushMatrix();

	glTranslatef(position.getX(), position.getY(), 0);

	glBegin( GL_QUADS );
		glTexCoord2f(1.0, 0.0);
		glVertex2f( -(size.getX()), -(size.getY()) );

		glTexCoord2f(0.0, 0.0);
		glVertex2f( size.getX(), -(size.getY()) );

		glTexCoord2f(0.0, 1.0);
		glVertex2f( size.getX(),  size.getY() );

		glTexCoord2f(1.0, 1.0);
		glVertex2f( -(size.getX()), size.getY() );
	glEnd();

	glPopMatrix();
}

void phRenderer::drawBoundingBox(phVector2D position, phVector2D size)
{
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	drawSprite(position, size);
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void phRenderer::drawBoundingSphere(phVector2D pos, float radius)
{
	const short num_segments = 20;

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	float theta = 2 * 3.1415926 / float(num_segments); 
	float tangetial_factor = tanf(theta);//calculate the tangential factor 

	float radial_factor = cosf(theta);//calculate the radial factor 
	
	float x = radius;//we start at angle = 0 

	float y = 0; 
    
	glBegin(GL_LINE_LOOP); 
	for(int ii = 0; ii < num_segments; ii++) 
	{ 
		glVertex2f(x + pos.getX(), y + pos.getY());//output vertex 
        
		//calculate the tangential vector 
		//remember, the radial vector is (x, y) 
		//to get the tangential vector we flip those coordinates and negate one of them 

		float tx = -y; 
		float ty = x; 
        
		//add the tangential vector 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
        
		//correct using the radial factor 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd(); 
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

 void phRenderer::drawBoundingBoxAABB(phVector2D min, phVector2D max)
 {
 	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	
 	glBegin( GL_QUADS );
 		glVertex2f( min.getX(), min.getY() );
 
 		glVertex2f( max.getX(), min.getY());
 
 		glVertex2f( max.getX(),  max.getY() );
 
 		glVertex2f( min.getX(), max.getY() );
 	glEnd();
 
 	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
 }