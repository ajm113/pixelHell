#ifndef RENDERER_SETUP_HPP
#define RENDERER_SETUP_HPP

#include "Renderer.hpp"

class phRendererSetup : private phRenderer
{
public:
	phRendererSetup(phLogger* logger)
        : phRenderer(logger){}

     virtual ~phRendererSetup() {}

	bool render();
};

#endif