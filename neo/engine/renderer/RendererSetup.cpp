#include "RendererSetup.hpp"

bool phRendererSetup::render()
{
    //Initalize GL states
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    glDisable( GL_TEXTURE_2D );
    glDisable( GL_LIGHTING );
    //glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );
    glEnable( GL_COLOR_MATERIAL );

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

	//Initialize Projection Matrix
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

	if(this->checkGLErrors())
        return false;

    //Initialize Modelview Matrix
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

	if(this->checkGLErrors())
        return false;

	//Initialize clear color
	glClearColor( 0.f, 0.f, 0.f, 1.f );

	if(this->checkGLErrors())
        return false;

    //Do really basic OpenGL setup.
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);

    return true;
}