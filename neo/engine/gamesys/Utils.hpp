#ifndef UTILS_HPP
#define UTILS_HPP

#define SAFE_DELETE(x) { if(x) delete x; x = 0; }

#endif