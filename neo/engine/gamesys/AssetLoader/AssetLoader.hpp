#ifndef ASSET_LOADER_HPP
#define ASSET_LOADER_HPP

#include "../Logger.hpp"
#include "../Utils.hpp" //<<<< Maybe populare because of memory management.

class phAssetLoader
{
public:
	phAssetLoader(phLogger* logger, const std::string & modFolder = "")
	{
		setBaseFolder("base");
		m_logger = logger;

		if(modFolder.length())
		{
			setModFolder(modFolder);
			setUseModFolder(true);
		}
	}

	virtual ~phAssetLoader(){};

	void setBaseFolder(const std::string & x) { m_baseFolder = x;    }
	void setModFolder(const std::string & x)  { m_modFolder = x;     }
	void setUseModFolder(bool x)			  {  m_useModFolder = x; }

	std::string getBaseFolder()    const { return m_baseFolder;   }
	std::string getModFolder()     const { return m_modFolder;    }
	bool        isUsingMod()       const { return m_useModFolder; }

	std::string generateAssetPath(const std::string asset);

	virtual bool load(const std::string & file) = 0;

	protected:
		phLogger* m_logger;
	private:
		std::string m_baseFolder;
		std::string m_modFolder;
		bool        m_useModFolder;

};


#endif