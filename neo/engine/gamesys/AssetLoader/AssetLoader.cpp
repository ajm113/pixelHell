#include "AssetLoader.hpp"

std::string phAssetLoader::generateAssetPath(const std::string asset)
{
	if(!isUsingMod()) {
		if(getBaseFolder().length())
			return getBaseFolder() + "/" + asset;
		return asset;
	}

	//Ensure we can actully open the file with the mod.
	std::string modPath = getModFolder() + "/" + asset;
	FILE* fp = fopen(modPath.c_str(), "r");

	if(!fp)
	{
		if(getBaseFolder().length())
			return getBaseFolder() + "/" + asset;
		return asset;
	}

	fclose(fp);

	return modPath;
}