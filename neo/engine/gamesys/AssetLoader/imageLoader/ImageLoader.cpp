#include "ImageLoader.hpp"

bool phImageLoader::load(const std::string & file)
{
	std::string imagePath = generateAssetPath(file);

	if(imagePath.length() < 3)
	{
		m_logger->printWarning("Failed loading '%s'! Invalid path!", imagePath.c_str());
		return false;
	}

	m_logger->printDebug("Loading image '%s'...", imagePath.c_str());

	FILE* fp = fopen(imagePath.c_str(), "rb");

	if(!fp)
	{
		m_logger->printWarning("Failed loading '%s'! Invalid path!", imagePath.c_str());
		return false;
	}

	//Initalize image pointer.
	phImage* image = new phImage();
	if(!image)
	{
		m_logger->printWarning("Could not initalize image pointer! Out of memory error!");
		return false;
	}

	image->setName(imagePath);

	std::string imageExtension = imagePath.substr(imagePath.find_last_of(".") + 1);

	//Perhaps add more image support?
	if(imageExtension == "ppm")
	{
		if(!loadPPMImage(fp, image))
			return false;
	}
	else if(imageExtension == "tga")
	{
		phTGALoader* tgaLoader = new phTGALoader(m_logger);
		bool succesfulLoader = tgaLoader->load(fp, image);
		SAFE_DELETE(tgaLoader);

		if(!succesfulLoader)
			return false;
	}
	else
	{
		m_logger->printWarning("Invalid file type '%s'! PPM and TGA formats are only supported!", imagePath.c_str());
		return false;
	}

	generateGLImage(image);

	m_imageList.push_back(image);

	fclose(fp);

	setLastImage(image);

	return true;
}

bool phImageLoader::loadPPMImage(FILE* fp, phImage* image)
{
	unsigned int i, width, height, d = 0;
	char head[70];		// max line <= 70 in PPM (per spec).

	fgets(head, sizeof(head), fp);	//Get header.

	if (strncmp(head, "P6", 2))
	{
		m_logger->printWarning("Not a raw PPM file '%s'!", image->getName().c_str());
		return false;
	}

	// Grab the three elements in the header (width, height, maxval).
	while( i < 3 ) {
		fgets( head, sizeof(head), fp );
		if ( head[0] == '#' )		// skip comments.
		continue;
		if ( i == 0 )
		i += sscanf( head, "%d %d %d", &width, &height, &d );
		else if ( i == 1 )
		i += sscanf( head, "%d %d", &height, &d );
		else if ( i == 2 )
		i += sscanf( head, "%d", &d );
	}

	// Grab all the image data in one fell swoop.
	unsigned char* imageDataBuffer = (unsigned char*) malloc( sizeof( unsigned char ) * width * height * 3 );
	fread( imageDataBuffer, sizeof( unsigned char ), width * height * 3, fp );

	//Map the image pointer.
	image->setHeight(height);
	image->setWidth(width);
	image->setBuffer(imageDataBuffer);
	image->setBitsPerPixel(3);

	return true;
}

void phImageLoader::generateGLImage(phImage* image)
{
	glEnable( GL_TEXTURE_2D );

	//Generate GL texture ID.
	GLuint textureId = 0;
	glGenTextures(1, &textureId);
	glBindTexture( GL_TEXTURE_2D, textureId );
	image->setGLId((unsigned int)textureId);

	if(checkGLErrors())
		return;

	GLubyte* texture = (GLubyte*) image->getBuffer(); //Cast unsigned char* to GLubyte*!

	GLuint imageType = (image->hasAlpha()) ? GL_RGBA : GL_RGB;

	gluBuild2DMipmaps( GL_TEXTURE_2D, image->getBitsPerPixel() / 8, image->getWidth(), image->getHeight(), imageType, GL_UNSIGNED_BYTE, texture );
	SAFE_DELETE(texture);

	checkGLErrors();

	glDisable( GL_TEXTURE_2D );
}

void phImageLoader::clearAll()
{
	int texturesSize = m_imageList.size();

	for(int i = 0; i<texturesSize; i++)
	{
		SAFE_DELETE(m_imageList.at(i));
	}

	m_imageList.clear();
}

bool phImageLoader::checkGLErrors()
{
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		m_logger->printWarning( "OpenGL Error: %s", gluErrorString( error ));
		return true;
	}

	return false;
}