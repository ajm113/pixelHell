#ifndef IMAGE_LOADER_HPP
#define IMAGE_LOADER_HPP

#include <stdlib.h>
#include <vector>

#include "../../Logger.hpp"
#include "../AssetLoader.hpp"
#include "Image.hpp"
#include "TGALoader.hpp"

class phImageLoader : public phAssetLoader
{
public:
	phImageLoader(phLogger* logger, const std::string & modFolder = "") :
	phAssetLoader(logger, modFolder)
	{
		m_lastImage = NULL;
	}

	~phImageLoader() { clearAll(); }
	
	bool load(const std::string & file);

	phImage* getLastImage() const { return m_lastImage;  }
	void setLastImage(phImage* x) { m_lastImage = x; }

	void clearAll();

	bool checkGLErrors();

protected:
	std::vector<phImage*> m_imageList;
	phImage*   		  m_lastImage;
private:
	bool     loadPPMImage(FILE* fp, phImage* image);
	void     generateGLImage(phImage* image);
};

#endif