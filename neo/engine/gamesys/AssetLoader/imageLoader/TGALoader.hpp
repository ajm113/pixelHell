#ifndef TGA_LOADER_HPP
#define TGA_LOADER_HPP

typedef struct
{
    unsigned char header[12];         // File Header To Determine File Type
} phTGAHeader;

#include <stdlib.h>

#include "../../Logger.hpp"
#include "../../Utils.hpp"
#include "Image.hpp"

class phTGALoader {
public:

	phTGALoader(phLogger* logger)
	{
		m_logger = logger;
	}

	// Uncompressed TGA Header
	static const unsigned char UNCOMPRESSED_TGA[12];
	// Compressed TGA Header
	static const unsigned char COMPRESSED_TGA[12];

	bool load(FILE* fp, phImage* image);

protected:
	phLogger* m_logger;
private:

	bool loadCompressed(FILE* fp, phImage* image);
	bool loadUncompressed(FILE* fp, phImage* image);
	bool readHeaderAndAllocateBuffer(FILE* fp, phImage* image, unsigned int &bytesPerPixel, unsigned int & imageSize, unsigned char* & buffer);

	/* Reading compressed TGA methods. */
	bool readCompressedRawColor(FILE* fp, unsigned char chunkHeader, unsigned int &currentByte, 
	unsigned int bytesPerPixel, unsigned int &currentPixel, unsigned char* colorBuffer, unsigned char* & buffer, unsigned int pixelCount);
	bool readCompressedNonRawColor(FILE* fp, unsigned char chunkHeader, unsigned int &currentByte, 
	unsigned int bytesPerPixel, unsigned int &currentPixel, unsigned char* colorBuffer, unsigned char* & buffer, unsigned int pixelCount);

};

#endif