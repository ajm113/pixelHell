#include "TGALoader.hpp"

const unsigned char phTGALoader::UNCOMPRESSED_TGA[12] = {0,0, 2,0,0,0,0,0,0,0,0,0};
const unsigned char phTGALoader::COMPRESSED_TGA[12] = {0,0,10,0,0,0,0,0,0,0,0,0};

bool phTGALoader::load(FILE* fp, phImage* image)
{
	phTGAHeader tgaHeader;

	if(!fread(&tgaHeader, sizeof(phTGAHeader), 1, fp))	// Attempt to read 12 byte header from file
	{
		m_logger->printError("Could not read file header '%s'", image->getName().c_str());
		return false;														
	}

	if(!memcmp(UNCOMPRESSED_TGA, &tgaHeader, sizeof(tgaHeader)))		// See if header matches the predefined header of 
	{														// an Uncompressed TGA image
		return loadUncompressed(fp, image);						// If so, jump to Uncompressed TGA loading code
	}
	else if(!memcmp(COMPRESSED_TGA, &tgaHeader, sizeof(tgaHeader)))	// See if header matches the predefined header of
	{														// an RLE compressed TGA image
		return loadCompressed(fp, image);							// If so, jump to Compressed TGA loading code
	}
	
	m_logger->printWarning("TGA file be type 2 or type 10! '%s'", image->getName().c_str());

	return false;
}

bool phTGALoader::loadCompressed(FILE* fp, phImage* image)
{
	unsigned int   bytesPerPixel = 0;
	unsigned int   imageSize     = 0;
	unsigned char* buffer        = NULL;

	if(!readHeaderAndAllocateBuffer(fp, image, bytesPerPixel, imageSize, buffer))
		return false;

	unsigned int pixelCount	  = image->getHeight() * image->getWidth();		// Nuber of pixels in the image
	unsigned int currentPixel  = 0;									// Current pixel being read
	unsigned int currentByte	  = 0;									// Current byte 
	unsigned char* colorBuffer = (GLubyte *)malloc(bytesPerPixel);			// Storage for 1 pixel

	do
	{
		unsigned char chunkHeader = 0;											// Storage for "chunk" header

		if(fread(&chunkHeader, sizeof(unsigned char), 1, fp) == 0)				// Read in the 1 byte header
		{
			m_logger->printWarning("Could not read RLE header! '%s'", image->getName().c_str());
			return false;													// Return failed
		}

		if(chunkHeader < 128)	// If the ehader is < 128, it means the that is the number of RAW color packets minus 1
		{
			chunkHeader++;		// add 1 to get number of following color values
			if(!readCompressedRawColor(fp, chunkHeader, currentByte, bytesPerPixel, currentPixel, colorBuffer, buffer, pixelCount))
			{
				SAFE_DELETE(colorBuffer);
				SAFE_DELETE(buffer);
				return false;
			}

			continue;
		}

		chunkHeader -= 127;
		if(!readCompressedNonRawColor(fp, chunkHeader, currentByte, bytesPerPixel, currentPixel, colorBuffer, buffer, pixelCount))
		{
			SAFE_DELETE(colorBuffer);
			SAFE_DELETE(buffer);
			return false;	
		}
	}
	while(currentPixel < pixelCount);

	image->setBuffer(buffer);

	return true;
}

bool phTGALoader::loadUncompressed(FILE* fp, phImage* image)
{
	unsigned int   bytesPerPixel = 0;
	unsigned int   imageSize     = 0;
	unsigned char* buffer        = NULL;

	if(!readHeaderAndAllocateBuffer(fp, image, bytesPerPixel, imageSize, buffer))
		return false;

	if(fread(buffer, 1, imageSize, fp) != imageSize)	// Attempt to read image data
	{
		m_logger->printWarning("Could not read TGA data! '%s'", image->getName().c_str());
		return false;														// Return failed
	}

	for(unsigned int i = 0; i < imageSize; i += bytesPerPixel)
	{
		buffer[i] ^= buffer[i+2] ^= buffer[i] ^= buffer[i+2];
	}

	image->setBuffer(buffer);

	return true;
}

bool phTGALoader::readHeaderAndAllocateBuffer(FILE* fp, phImage* image, unsigned int &bytesPerPixel, unsigned int & imageSize, unsigned char* & buffer)
{
	unsigned char header[6];

	if(!fread(header, sizeof(header), 1, fp))	// Read TGA header
	{					
		m_logger->printWarning("Could not read info header! '%s'", image->getName().c_str());
		return false;											
	}

	/* Map out the header data to our image model.  */
	image->setWidth( header[1] * 256 + header[0] );	// Determine The TGA Width	(highbyte*256+lowbyte)
	image->setHeight( header[3] * 256 + header[2] );	// Determine The TGA Height	(highbyte*256+lowbyte)
	image->setBitsPerPixel( header[4] );			// Determine the bits per pixel

	/* Do some basic error checking... */
	if(!image->getWidth())
	{
		m_logger->printWarning("Invalid image information on width! '%s'", image->getName().c_str());
		return false;		
	}

	if(!image->getHeight())
	{
		m_logger->printWarning("Invalid image information on height! '%s'", image->getName().c_str());
		return false;
	}

	if(image->getBitsPerPixel() != 24 && image->getBitsPerPixel() != 32)
	{
		m_logger->printWarning("Unsupported bits per pixel %i! 24 and 32 are supported! '%s'", image->getBitsPerPixel(), image->getName().c_str());
		return false;
	}

	/* Does this TGA have a alpha channel? */
	if(image->getBitsPerPixel() == 32)
		image->setHasAlpha(true);
	

	bytesPerPixel = (image->getBitsPerPixel() / 8);							// Compute the number of BYTES per pixel
	imageSize	    = (bytesPerPixel * image->getWidth() * image->getHeight());		// Compute the total amout ofmemory needed to store data
	buffer	    = (unsigned char*)malloc(imageSize);						// Allocate that much memory

	if(!buffer)
	{
		m_logger->printWarning("Out of memory error for TGA image: (Too big?) '%s'", image->getName().c_str());
		return false;
	}

	return true;
}

bool phTGALoader::readCompressedRawColor(FILE* fp, unsigned char chunkHeader, unsigned int &currentByte, 
	unsigned int bytesPerPixel, unsigned int &currentPixel, unsigned char* colorBuffer, unsigned char* & buffer, unsigned int pixelCount)
{
	for(short counter = 0; counter < chunkHeader; counter++)		// Read RAW color values
	{
		if(fread(colorBuffer, 1, bytesPerPixel, fp) != bytesPerPixel)	// Try to read 1 pixel
		{
			m_logger->printWarning("Could not read image data!");
			return false;		
		}											
														// write to memory
		buffer[currentByte    ] = colorBuffer[2];				// Flip R and B vcolor values around in the process 
		buffer[currentByte + 1] = colorBuffer[1];
		buffer[currentByte + 2] = colorBuffer[0];

		if(bytesPerPixel == 4)								// if its a 32 bpp image
			buffer[currentByte + 3] = colorBuffer[3];			// copy the 4th byte
		
		currentByte += bytesPerPixel;							// Increase thecurrent byte by the number of bytes per pixel
		currentPixel++;									// Increase current pixel by 1

		if(currentPixel > pixelCount)											// Make sure we havent read too many pixels
		{
			m_logger->printWarning("Too many pixels to read!");
			return false;
		}
	}

	return true;
}

bool phTGALoader::readCompressedNonRawColor(FILE* fp, unsigned char chunkHeader, unsigned int &currentByte, 
	unsigned int bytesPerPixel, unsigned int &currentPixel, unsigned char* colorBuffer, unsigned char* & buffer, unsigned int pixelCount)
{
	if(fread(colorBuffer, 1, bytesPerPixel, fp) != bytesPerPixel)    // Try to read 1 pixel
	{
		m_logger->printWarning("Could not read image data!");
		return false;
	}

	for(short counter = 0; counter < chunkHeader; counter++)		// copy the color into the image data as many times as dictated 
	{													// by the header
		buffer[currentByte	  ] = colorBuffer[2];				// switch R and B bytes areound while copying
		buffer[currentByte + 1] = colorBuffer[1];
		buffer[currentByte + 2] = colorBuffer[0];

		if(bytesPerPixel == 4)									// If TGA images is 32 bpp
			buffer[currentByte + 3] = colorBuffer[3];				// Copy 4th byte

		currentByte += bytesPerPixel;								// Increase current byte by the number of bytes per pixel
		currentPixel++;										// Increase pixel count by 1

		if(currentPixel > pixelCount)								// Make sure we havent read too many pixels
		{
			m_logger->printWarning("Too many pixels to read!");
			return false;
		}
	}

	return true;
}