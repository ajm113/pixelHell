#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>
#include <GL\GLU.h>

class phImage
{
public:
	phImage()
	{
		setName("#undefined");
		setGLId(0);
		setHeight(0);
		setWidth(0);
		setBuffer(NULL);
		setHasAlpha(false);
	}

	~phImage()
	{
		if(getGLId())
			glDeleteTextures(1, &m_glId);
	}

	void setName(const std::string & x)          { m_name = x;         }
	void setHeight(unsigned int x)               { m_height = x;       }
	void setWidth(unsigned int x)                { m_width = x;        }
	void setGLId(unsigned int x)                 { m_glId = x;         }
	void setBuffer(unsigned char* x)             { m_buffer = x;       }
	void setBitsPerPixel(unsigned int x)         { m_bitsPerPixel = x; }
	void setHasAlpha(bool x)                     { m_hasAlpha = x;     }

	std::string    getName()          const {  return m_name;         }
	unsigned int   getHeight()        const {  return m_height;       }
	unsigned int   getWidth()         const {  return m_width;        }
	unsigned int   getGLId()          const {  return m_glId;         }
	unsigned char* getBuffer()        const {  return m_buffer;       }
	unsigned int   getBitsPerPixel()  const {  return m_bitsPerPixel; }
	bool           hasAlpha()         const {  return m_hasAlpha;     }

protected:
	std::string    m_name;
	unsigned int   m_height;
	unsigned int   m_width;
	unsigned char* m_buffer;
	unsigned int   m_glId;
	unsigned int   m_bitsPerPixel;
	bool           m_hasAlpha;
};

#endif