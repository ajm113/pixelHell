#ifndef LUA_ENGINE_FUNCTIONS_HPP
#define LUA_ENGINE_FUNCTIONS_HPP

#include "../../EngineCore.hpp"

int phBindFunction(lua_State *L);
int phPrintErrorFunction(lua_State *L);
int phPrintWarningFunction(lua_State *L);
int phPrintMessageFunction(lua_State *L);
int phPrintDebugFunction(lua_State *L);


#endif