#ifndef LUA_HANDLER_HPP
#define LUA_HANDLER_HPP

#include "lua.hpp"

extern "C" {
#include "lauxlib.h"
#include "lualib.h"
}

#include "../AssetLoader.hpp"
#include "../../Logger.hpp"

class phLuaLoader : public phAssetLoader {
public:

	phLuaLoader(const phLuaLoader& other); //non-construction copy
	phLuaLoader& operator=(const phLuaLoader&); //non-copy

	phLuaLoader(phLogger* logger, const std::string & modFolder = "") :
	phAssetLoader(logger, modFolder)
	{
		m_lua = luaL_newstate();
		luaopen_io(m_lua); // provides io.*
		luaopen_base(m_lua);
		luaopen_string(m_lua);
		luaopen_math(m_lua);
	}

	~phLuaLoader()
	{
		clearAll();
	}

	lua_State* getState()
	{
		return m_lua;
	}

	int getGlobalInt(const std::string & x)
	{
		lua_getglobal(m_lua, x.c_str());

		if(lua_isnil(m_lua,-1)){
			m_logger->printError("Lua error: NIL/NULL from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}

		if(!lua_isnumber(m_lua, -1)) {
			m_logger->printError("Lua error: Expecting number from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}
		int v =  (int)lua_tonumber(m_lua, -1);
		lua_pop(m_lua, 1);
		return v;
	}

	bool getGlobalBoolean(const std::string & x)
	{
		lua_getglobal(m_lua, x.c_str());

		if(lua_isnil(m_lua,-1)){
			m_logger->printError("Lua error: NIL/NULL from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}

		if(!lua_isboolean(m_lua, -1)) {
			m_logger->printError("Lua error: Expecting boolean from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}
		bool v =  (bool)lua_toboolean(m_lua, -1);
		lua_pop(m_lua, 1);
		return v;
	}

	std::string getGlobalString(const std::string & x)
	{
		lua_getglobal(m_lua, x.c_str());

		if(lua_isnil(m_lua,-1)){
			m_logger->printError("Lua error: NIL/NULL from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}

		if(!lua_isstring(m_lua, -1)) {
			m_logger->printError("Lua error: Expecting boolean from '%s'!", x.c_str());
			lua_pop(m_lua, 1);
			return 0;
		}
		std::string v =  lua_tostring(m_lua, -1);
		lua_pop(m_lua, 1);
		return v;
	}

	bool registerFunction(const std::string & name, lua_CFunction callbackFunction)
	{
		lua_pushcfunction(m_lua, callbackFunction);
		lua_setglobal(m_lua, name.c_str());
		return true;
	}

	bool load(const std::string & file)
	{
		if(luaL_loadfile(m_lua, generateAssetPath(file).c_str()))
			return false;

		return execute();
	}

	bool execute()
	{
		int response = lua_pcall(m_lua, 0, LUA_MULTRET, 0);
		reportErrors(response);
		return (!response);
	}

	void clearAll()
	{
		if(m_lua)
		{
			lua_close(m_lua);
		}
	}

protected:
	lua_State* m_lua;
private:

	//helper function to report errors in evaluated lua scripts
	void reportErrors( int state )
	{
		if(state != 0)
		{
			m_logger->printError(lua_tostring(m_lua, -1));
			lua_pop(m_lua, 1); //remove error
		}
	}
};

#endif