#include "LuaEngineFunctions.hpp"

int phBindFunction(lua_State *L)
{
	int argc = lua_gettop(L);

	if(argc != 2)
	{
		lua_pushstring(L, "Incorect arguments given for bind. Expected 2 arguments.");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printError("Expected string for bind!");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 2)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printError("Expected strings for bind!");
		lua_error(L);
		return 0;
	}


	SDL_Keycode kc = SDL_GetKeyFromName(lua_tostring(L, 1));

	if(kc == SDLK_UNKNOWN)
	{
		engine->getLogger()->printError("Unkown key code '%s'!", lua_tostring(L, 1));
		return 0;
	}

	phKeyEvent e;
	e.setName(lua_tostring(L, 2));
	e.setKey(kc);

	engine->getKeyMapper()->registerKeyEvent(e);

	return 0;
}

int phPrintErrorFunction(lua_State *L)
{
	int argc = lua_gettop(L);

	if(argc != 1)
	{
		lua_pushstring(L, "Incorect arguments given. Expected 1 argument.");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printError("Expected string for printError!");
		lua_error(L);
		return 0;
	}

	engine->getLogger()->printError(lua_tostring(L, 1));
	return 0;
}

int phPrintWarningFunction(lua_State *L)
{
	int argc = lua_gettop(L);

	if(argc != 1)
	{
		lua_pushstring(L, "Incorect arguments given. Expected 1 argument.");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printError("Expected string for printWarning!");
		lua_error(L);
		return 0;
	}

	engine->getLogger()->printWarning(lua_tostring(L, 1));
	return 0;
}

int phPrintMessageFunction(lua_State *L)
{
	int argc = lua_gettop(L);

	if(argc != 1)
	{
		lua_pushstring(L, "Incorect arguments given. Expected 1 argument.");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printMessage("Expected string for printMessage!");
		lua_error(L);
		return 0;
	}

	engine->getLogger()->printWarning(lua_tostring(L, 1));
	return 0;
}

int phPrintDebugFunction(lua_State *L)
{
	int argc = lua_gettop(L);

	if(argc != 1)
	{
		lua_pushstring(L, "Incorect arguments given. Expected 1 argument.");
		lua_error(L);
		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected string.");
		engine->getLogger()->printError("Expected string for printDebug!");
		lua_error(L);
		return 0;
	}

	engine->getLogger()->printDebug(lua_tostring(L, 1));
	return 0;
}