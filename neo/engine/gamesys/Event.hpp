#ifndef EVENT_HANDLER_HPP
#define EVENT_HANDLER_HPP

#include "EventDispatcher.hpp"


class phClass;
class phEventDispatcher;

/* Event Types */

class phEvent
{
public:

	enum phEventType
	{
		E_NONE = 1,

		//Keyboard  / Mouse events.
		E_LEFT_MOUSE_BUTTON_PRESS,
		E_LEFT_MOUSE_BUTTON_RELEASE,
		E_RIGHT_MOUSE_BUTTON_PRESS,
		E_RIGHT_MOUSE_BUTTON_RELEASE,

		E_MOUSEMOVE,

		E_KEYDOWN,
		E_KEYUP,

		//Game Events
		E_GAME_PAUSE,
		E_GAME_START,
		E_GAME_CONTINUE,
		E_GAME_OVER,


		//Phyiscs Events
		E_PHYSICS_TOUCH,
		E_PHYSICS_TOUCH_END,


		//Game is closing
		E_APP_CLOSE,

		//Game initalized fully and ready to go!
		E_APP_STARTED

	};


	phEvent(phEventType type = E_NONE, int arg1 = 0, int arg2 = 0, phClass* obj = 0){ m_type = type; m_arg1 = arg1; m_arg2 = arg2; m_object = obj; }
	~phEvent() {};

	phEventType getType()   { return m_type; }
	int         getArg1()   { return m_arg1; }
	int         getArg2()   { return m_arg2; }
	phClass*    getObject() { return m_object; }

protected:

	phEventType m_type;
	int         m_arg1;
	int         m_arg2;
	phClass*    m_object;
};


class phEventHandler {
public:
	virtual void eventHandler(phEvent event) = 0;

	// Mutator and selector
	phEventHandler * getNextHandler() { return _nextHandler; }
	void setNextHandler(phEventHandler *next) { _nextHandler = next;}

	phEventHandler() : _nextHandler(0) {
		phEventDispatcher::get()->registerHandler(this);
	}

protected:
	void SendEvent(phEvent event) {
		phEventDispatcher::get()->sendEvent(event);
	}

private:
	phEventHandler *_nextHandler;
};



#endif