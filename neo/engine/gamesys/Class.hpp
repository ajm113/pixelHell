#ifndef CLASS_HPP
#define CLASS_HPP

#include "Event.hpp"
#include "EventDispatcher.hpp"
/*
	phClass Used for savable game objects.
*/
class phEventHandler;

class phClass : protected phEventHandler {
public:

	virtual void eventHandler(phEvent event) = 0;

protected:
private:
	
};

#endif