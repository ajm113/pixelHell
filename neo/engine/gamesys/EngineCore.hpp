#ifndef ENGINE_CORE_HPP
#define ENGINE_CORE_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <stdio.h>
#include <string>
#include <vector>

#include "Utils.hpp"
#include "Logger.hpp"
#include "Timer.hpp"
#include "EventDispatcher.hpp"
#include "KeyMapper.hpp"
#include "../renderer/RendererSetup.hpp"
#include "assetLoader/imageLoader/ImageLoader.hpp"
#include "assetLoader/luaLoader/luaLoader.hpp"
#include "assetLoader/luaLoader/LuaEngineFunctions.hpp"

/* Our physics engine! */
#include "../physics/Physics.hpp"
#include "../physics/PhysicsObject.hpp"

/* Include Entities */
#include "../entities/Player.hpp"

/*
	phEngineCore Main game instance / OpenGL setup / SDL setup.
*/

class phEngineCore {
public:
	phEngineCore() { m_windowTitle = "Untitled"; }
	~phEngineCore(){ clearAll(); }

	void setWindowTitle(const std::string & title) {  m_windowTitle = title; }
	const std::string & getWindowTitle() { return m_windowTitle; }

	void setModFolder(const std::string & mod){ m_modFolder = mod; }
	const std::string & getModFolder(){ return m_modFolder; }

	bool initialize();
	void clearAll();

	void run();

	/* Getter functions (Usually for Lua Binding Calls.) */
	phKeyMapper* getKeyMapper() { return m_keyMapper; }
	phLogger* getLogger() { return m_logger; }


protected:
	void render();
	void calculate();

	float getDeltaTime() { return m_deltaTime; }
	void setDeltaTime(float delta){ m_deltaTime = delta; }
private:

	phLuaLoader* loadConfigurationFile();
	bool loadKeyMapFile();

	void setupGame();

	static const int OPENGL_VERSION_MAJOR = 2;
	static const int OPENGL_VERSION_MINOR = 1;

	static const int SCREEN_SIZE_WIDTH    = 640;
	static const int SCREEN_SIZE_HEIGHT   = 480;
	static const int FRAMES_PER_SECOND    = 60;

	SDL_Window*       m_window;
	SDL_GLContext     m_glContext;
	phLogger*         m_logger;
	phTimer           m_timer;
	phTimer           m_fps;
	phPhysics*        m_physics;
	phKeyMapper*      m_keyMapper;
	phImageLoader*    m_imageLoader;

	float          m_deltaTime;

	std::string    m_windowTitle;
	std::string    m_modFolder;

	/* Render vector list */
	std::vector<phEntity*> m_entityStack;

	/* Temp Variables -- Please remove me! */
	phEntity*		m_player;
};

extern phEngineCore* engine;

#endif