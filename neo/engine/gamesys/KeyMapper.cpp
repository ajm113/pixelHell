#include "KeyMapper.hpp"

bool phKeyMapper::registerKeyEvent(phKeyEvent event)
{
	/* Can we add this event? */
	int eventSize = m_events.size();
	for(int i = 0; i<eventSize; i++)
	{
		const phKeyEvent e = m_events.at(i);

		if(e.getName() == event.getName() && e.getKey() == event.getKey())
			return false;
	}

	m_events.push_back(event);
	return true;
}

void phKeyMapper::keyPressed(const char key)
{
	int eventSize = m_events.size();
	for(int i = 0; i<eventSize; i++)
	{
		const phKeyEvent e = m_events.at(i);

		if(e.getKey() == key)
			m_events.at(i).setFiring(true);
	}
}

void phKeyMapper::keyUnpressed(const char key)
{
	int eventSize = m_events.size();
	for(int i = 0; i<eventSize; i++)
	{
		const phKeyEvent e = m_events.at(i);

		if(e.getKey() == key)
			m_events.at(i).setFiring(false);
	}
}

bool phKeyMapper::isEventFired(const std::string & name)
{
	int eventSize = m_events.size();
	for(int i = 0; i<eventSize; i++)
	{
		const phKeyEvent e = m_events.at(i);

		if(e.getName() == name && e.isFiring())
			return true;
	}

	return false;
}

void phKeyMapper::resetAll()
{
	int eventSize = m_events.size();
	for(int i = 0; i<eventSize; i++)
	{
		m_events.at(i).setFiring(false);
	}
}