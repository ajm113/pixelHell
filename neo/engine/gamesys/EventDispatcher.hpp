#ifndef EVENT_DISPATCHER_HPP
#define EVENT_DISPATCHER_HPP

#include <assert.h>

class phEventHandler;
class phEvent;

class phEventDispatcher {
public:
	static phEventDispatcher*	get();
	// These are private so that only instance will ever be created
	// the _deviceList pointer is initialized to null as the linked list
	// is null-terminated
	phEventDispatcher(void) : _deviceList(0) {;}
	~phEventDispatcher(void) {;}

	void registerHandler(phEventHandler *device);

	// Sends the event to all the devices registered to listen
	void sendEvent(phEvent e);

	// pointer to singleton class
	static phEventDispatcher*	_instanceOf;

private:
  phEventHandler *_deviceList;
};

#endif