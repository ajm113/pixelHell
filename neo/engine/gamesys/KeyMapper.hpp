#ifndef KEY_MAPPER_HPP
#define KEY_MAPPER_HPP

#include <string>
#include <vector>

class phKeyEvent
{
public:
	phKeyEvent() {
		setFiring(false);
		setKey(0);
		setName("#unkown");
	}

	std::string   getName() const { return m_name;  }
	char          getKey()  const { return m_key;   }
	bool          isFiring() const { return m_isFiring; }

	void setName(const std::string & x) { m_name = x;     }
	void setKey(const char & x)         { m_key = x;      }
	void setFiring(const bool x)        { m_isFiring = x; }
protected:
	std::string m_name;
	char        m_key;
	bool        m_isFiring;
};

class phKeyMapper
{
public:
	
	bool registerKeyEvent(phKeyEvent event);
	void keyPressed(const char key);
	void keyUnpressed(const char key);
	bool isEventFired(const std::string & name);
	void resetAll();

	std::vector<phKeyEvent> getEvents() const{ return m_events; }
	void setEvents(std::vector<phKeyEvent> x) { m_events = x; }

protected:
private:

	std::vector<phKeyEvent> m_events;

};


#endif