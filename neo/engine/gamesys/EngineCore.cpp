#include "EngineCore.hpp"

bool phEngineCore::initialize()
{
	/* Initialize logger! */
	m_logger = new phLogger("log.txt", phLogger::VERBOSE_LEVEL_ERROR);

	phLuaLoader* configurationFile = loadConfigurationFile();

     m_logger->printMessage( "Starting Pixel Hell Engine..." );

	/* Initialize SDL! */
	if(SDL_Init( SDL_INIT_VIDEO ) != 0)
	{
		m_logger->printError( "SDL could not initialize! SDL Error: %s", SDL_GetError() );
		SAFE_DELETE(configurationFile);
		return false;
	}

	/* Set our OpenGL version */
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, phEngineCore::OPENGL_VERSION_MAJOR );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, phEngineCore::OPENGL_VERSION_MINOR );

     /* Create window. */
     m_window = SDL_CreateWindow( m_windowTitle.c_str(), 
     	SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
     	phEngineCore::SCREEN_SIZE_WIDTH, phEngineCore::SCREEN_SIZE_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

     if(!m_window)
     {
		m_logger->printError( "Window could not be created! SDL Error: %s", SDL_GetError() );
		SAFE_DELETE(configurationFile);
		return false;
     }

     /* Initialize key mapper. */
     m_keyMapper = new phKeyMapper();
     if(!m_keyMapper)
     {
		m_logger->printError( "Could not initialize key mapper! Out of memory!");
		SAFE_DELETE(configurationFile);
		return false;
     }

     /* Initialize image manager. */
     m_imageLoader = new phImageLoader(m_logger, getModFolder());
     if(!m_imageLoader)
     {
		m_logger->printError( "Could not initialize image loader! Out of memory!");
		SAFE_DELETE(configurationFile);
		return false;
     }

	/* Create OpenGL context. */
	m_glContext = SDL_GL_CreateContext( m_window );

	if(!m_glContext)
	{
		m_logger->printError( "OpenGL context could not be created! SDL Error: %s", SDL_GetError() );
		SAFE_DELETE(configurationFile);
		return false;
	}

	if(configurationFile->getGlobalBoolean("vsync"))
	{
		m_logger->printMessage("V-Sync Enabled!");
		if(SDL_GL_SetSwapInterval( 1 ) < 0 )
		{
			m_logger->printWarning( "Unable to set VSync! SDL Error: %s", SDL_GetError() );
		}
	}
	else
	{
		m_logger->printMessage("V-Sync Disabled!");
	}

	m_physics = new phPhysics(m_logger);
	m_physics->setDebugMode(true);

	/* Setup / Check OpenGL */
	phRendererSetup* glSetup = new phRendererSetup(m_logger);
	if(!glSetup->render())
	{
		m_logger->printError( "Unable to initialize OpenGL!" );
		SAFE_DELETE(configurationFile);
		return false;
	}

	if(!loadKeyMapFile())
	{
		m_logger->printError( "Unable to load key map file!" );
	}

	SAFE_DELETE(glSetup);
	SAFE_DELETE(configurationFile);

	SDL_StartTextInput();

	setupGame();

	return true;
}

void phEngineCore::setupGame()
{
	/* Setup the player. May have to move this...*/
	m_player = new phPlayer(m_logger, m_keyMapper);
	m_player->setSize(phVector2D(0.04f, 0.04f));
	m_player->setMass(1.0);
	m_player->setCollisionModel(phPhysicsObject::MODEL_SPHERE);

	if(m_imageLoader->load("sprites/player.tga"))
		m_player->setImage(m_imageLoader->getLastImage());

	/* Move the player entity into the stack. */
	m_entityStack.push_back(m_player);
	m_physics->addObject(m_player);

	/* Create the 'room' for our player.  */
	phPhysicsObject* wall1 =  new phPhysicsObject();
	wall1->setPosition(phVector2D(-1.0,0.0));
	wall1->setSize(phVector2D(0.5,0.5));
	wall1->setSleep(true);
	wall1->setCollisionModel(phPhysicsObject::MODEL_AABB);

	m_physics->addObject(wall1);
}

void phEngineCore::clearAll()
{
	m_logger->printMessage( "Shutting down..." );

	SAFE_DELETE(m_keyMapper);
	SAFE_DELETE(m_imageLoader);
	SAFE_DELETE(m_logger);
	
	SDL_StopTextInput();

	if(m_window)
	{
		SDL_DestroyWindow( m_window );
		m_window = NULL;
	}

	size_t entityCount = m_entityStack.size();

	for(size_t i = 0; i < entityCount; i++)
	{
		SAFE_DELETE(m_entityStack.at(i));
	}

	m_entityStack.clear();

	/* Quit SDL instance! */
	SDL_Quit();
}

void phEngineCore::run()
{
	phEventDispatcher::get()->sendEvent(phEvent(phEvent::E_APP_STARTED));

	/* Move this to when we actully want to start the game. */
	phEventDispatcher::get()->sendEvent(phEvent(phEvent::E_GAME_START));

	bool quitLoop = false;
	while( !quitLoop )
	{
		/* Start the fps timer. */
		m_fps.start();

		/* Map SDL events to our own event handling system for better event mapping. */
		SDL_Event e;
		while( SDL_PollEvent( &e ) != 0 )
		{

			switch(e.type)
			{
				case SDL_QUIT:
					quitLoop = true;
					return;
				case SDL_KEYDOWN:
					m_keyMapper->keyPressed(e.key.keysym.sym);
					phEventDispatcher::get()->sendEvent(phEvent(phEvent::E_KEYDOWN, e.key.keysym.sym));
					break;
				case SDL_KEYUP:
					m_keyMapper->keyUnpressed(e.key.keysym.sym);
					phEventDispatcher::get()->sendEvent(phEvent(phEvent::E_KEYUP, e.key.keysym.sym));
					break;
				case SDL_MOUSEMOTION:
					phEventDispatcher::get()->sendEvent(phEvent(phEvent::E_MOUSEMOVE, e.motion.x, e.motion.y));
					break;
				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				{

					phEvent::phEventType pressState = phEvent::E_LEFT_MOUSE_BUTTON_PRESS;

					switch(e.button.button)
					{
						case SDL_BUTTON_LEFT:
						{
							pressState = phEvent::E_LEFT_MOUSE_BUTTON_PRESS;

							if(e.button.state == SDL_RELEASED)
								pressState = phEvent::E_LEFT_MOUSE_BUTTON_RELEASE;
							break;
						}
						case SDL_BUTTON_RIGHT:
						{
							pressState = phEvent::E_RIGHT_MOUSE_BUTTON_PRESS;

							if(e.button.state == SDL_RELEASED)
								pressState = phEvent::E_RIGHT_MOUSE_BUTTON_RELEASE;
							break;
						}
					}

					phEventDispatcher::get()->sendEvent(phEvent(pressState, e.motion.x, e.motion.y));
					break;
				}
			}
		}

		/* Update Entity Stack */
		calculate();

		/* Draw entities to screen. */
		render();

		/* Cap the framerate. */
		if( m_fps.getTicks() < 1000 / phEngineCore::FRAMES_PER_SECOND )
		{
			SDL_Delay( ( 1000 / phEngineCore::FRAMES_PER_SECOND ) - m_fps.getTicks() );
		}
	}
}

void phEngineCore::render()
{
	phRenderer::clearColorBuffer();

	size_t stackSize = m_entityStack.size();

	for(size_t i = 0; i<stackSize; i++)
	{
		m_entityStack.at(i)->render();
	}

	//Draw debugging output for our physics engine.
	m_physics->render();

	/* Update the context. */
	SDL_GL_SwapWindow( m_window );
}

void phEngineCore::calculate()
{
	/* Set delta time. */
	setDeltaTime(m_timer.getTicks() / 1000.f);

	/* Update any logic / AI for entities. */
	size_t stackSize = m_entityStack.size();
	for(size_t i = 0; i<stackSize; i++)
	{
		m_entityStack.at(i)->updateLogic(getDeltaTime());
	}

	/* Update our world! */
	m_physics->step(getDeltaTime());

	/* Reset timer. */
	m_timer.start();
}

phLuaLoader* phEngineCore::loadConfigurationFile()
{
     /* Initialize Lua */
     phLuaLoader* configurationFile = new phLuaLoader(m_logger);
     configurationFile->setBaseFolder("");

     if(!configurationFile)
     {
		m_logger->printError( "Could not initialize Lua! Out of memory!");
		return NULL;
     }

     /* Load the configuration file. */
     if(!configurationFile->load("config.lua"))
     {
     	SAFE_DELETE(configurationFile);
     	return NULL;
     }

     /* Configure our verbosity of the logger. */
     switch(configurationFile->getGlobalInt("loggingVerboseLevelConsole"))
     {
		case phLogger::VERBOSE_LEVEL_DEBUG:
			m_logger->setVerboseLevel(phLogger::VERBOSE_LEVEL_DEBUG);
			break;
		case phLogger::VERBOSE_LEVEL_MESSAGE:
			m_logger->setVerboseLevel(phLogger::VERBOSE_LEVEL_MESSAGE);
			break;
		case phLogger::VERBOSE_LEVEL_WARNING:
			m_logger->setVerboseLevel(phLogger::VERBOSE_LEVEL_WARNING);
			break;
		case phLogger::VERBOSE_LEVEL_ERROR:
			m_logger->setVerboseLevel(phLogger::VERBOSE_LEVEL_ERROR);
			break;
		default:
			m_logger->setVerboseLevel(phLogger::VERBOSE_LEVEL_MESSAGE);
			m_logger->printMessage("Unknown verbosity level set for console! Setting to MESSAGE.");
     }

     switch(configurationFile->getGlobalInt("loggingVerboseLevelFile"))
     {
		case phLogger::VERBOSE_LEVEL_DEBUG:
			m_logger->setVerboseLevelLogFile(phLogger::VERBOSE_LEVEL_DEBUG);
			break;
		case phLogger::VERBOSE_LEVEL_MESSAGE:
			m_logger->setVerboseLevelLogFile(phLogger::VERBOSE_LEVEL_MESSAGE);
			break;
		case phLogger::VERBOSE_LEVEL_WARNING:
			m_logger->setVerboseLevelLogFile(phLogger::VERBOSE_LEVEL_WARNING);
			break;
		case phLogger::VERBOSE_LEVEL_ERROR:
			m_logger->setVerboseLevelLogFile(phLogger::VERBOSE_LEVEL_ERROR);
			break;
		default:
			m_logger->setVerboseLevelLogFile(phLogger::VERBOSE_LEVEL_WARNING);
			m_logger->printMessage("Unknown verbosity level set for file! Setting to WARNING.");
     }

     return configurationFile;
}

bool phEngineCore::loadKeyMapFile()
{
     /* Initialize Lua */
     phLuaLoader* keyMapFile = new phLuaLoader(m_logger);

     if(!keyMapFile)
     {
		m_logger->printError( "Could not initialize Lua! Out of memory!");
		return false;
     }

     keyMapFile->setBaseFolder("");

     /* Set the bind function. */
     keyMapFile->registerFunction("bind", phBindFunction);

     /* Load the configuration file. */
     if(!keyMapFile->load("keymap.lua"))
     {
     	SAFE_DELETE(keyMapFile);
     	return false;
     }

     SAFE_DELETE(keyMapFile);
     return true;
}