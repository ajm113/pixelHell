#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <iostream>
#include <fstream>
#include <stdarg.h>

class phLogger {
public:

	enum phVerboseLevel {
		VERBOSE_LEVEL_DEBUG   = 1,
		VERBOSE_LEVEL_MESSAGE = 2,
		VERBOSE_LEVEL_WARNING = 3,
		VERBOSE_LEVEL_ERROR   = 4
	};

	phLogger(const char* fileName, phLogger::phVerboseLevel level = VERBOSE_LEVEL_DEBUG)
	{
		m_logStream.open(fileName);
		setVerboseLevel(level);
		setVerboseLevelLogFile(VERBOSE_LEVEL_ERROR);
		setLogFile(fileName);
	}

	~phLogger()
	{
		m_logStream.close();
		delete m_logFile;
	}

	void printError(const char* message, ...);
	void printWarning(const char* message, ...);
	void printMessage(const char* message, ...);
	void printDebug(const char* message, ...);

	void setVerboseLevel(phLogger::phVerboseLevel x)        {  m_verboseLevel = x;                                       }
	void setVerboseLevelLogFile(phLogger::phVerboseLevel x) {  m_verboseLevelLogFile = x;                                }
	void setLogFile(const char* x)                      { m_logFile = new char[strlen(x) + 1]; strcpy(m_logFile, x); }

	phLogger::phVerboseLevel getVerboseLevel()        { return m_verboseLevel;        }
	phLogger::phVerboseLevel getVerboseLevelLogFile() { return m_verboseLevelLogFile; }
	char* getLogFile()                                { return m_logFile;             }

protected:
	phLogger::phVerboseLevel m_verboseLevel;
	phLogger::phVerboseLevel m_verboseLevelLogFile;
	char*                m_logFile;
	void                 print(phLogger::phVerboseLevel level, const char* message);
private:

	void printToConsole(const char* message, phLogger::phVerboseLevel level);
	void printToLogFile(const char* message, phLogger::phVerboseLevel level);

	inline const char* verboseLevelToString(phLogger::phVerboseLevel level);


	std::ofstream m_logStream;

};

#endif