#ifndef COLOR_HPP
#define COLOR_HPP

class phColor
{
public:

	phColor(unsigned short r = 255, unsigned short g = 255, unsigned short b = 255, float a = 1.0 )
	{
		setRed(r);
		setGreen(g);
		setBlue(b);
		setAlha(a);
	}

	unsigned short getRed()   const {  return m_R; }
	unsigned short getGreen() const {  return m_G; }
	unsigned short getBlue()  const {  return m_B; }
	float          getAlpha() const {  return m_A; }

	void setRed(unsigned short   x) { m_R = (x < 255) ? x : 255; }
	void setGreen(unsigned short x) { m_G = (x < 255) ? x : 255; }
	void setBlue(unsigned short  x) { m_B = (x < 255) ? x : 255; }
	void setAlha(float  x) 		  { m_A = (x < 1.0) ? x : 1.0; }


	/* Operators */
	phColor& operator +=(const phColor& c)
	{
		m_R += c.getRed();
		m_G += c.getGreen();
		m_B += c.getBlue();
		m_A += c.getAlpha();
		return (*this);
	}

	phColor& operator -=(const phColor& c)
	{
		m_R -= c.getRed();
		m_G -= c.getGreen();
		m_B -= c.getBlue();
		m_A -= c.getAlpha();
		return (*this);
	}

	phColor operator +(const phColor& c) const
	{
		return (phColor(m_R + c.getRed(), m_G + c.getGreen(), m_B + c.getBlue(), m_A + c.getAlpha()));
	}

	phColor operator -(const phColor& c) const
	{
		return (phColor(m_R - c.getRed(), m_G - c.getGreen(), m_B - c.getBlue(), m_A - c.getAlpha()));
	}

	bool operator ==(const phColor& c) const
	{
		return ((m_R == c.getRed()) && (m_G == c.getGreen()) && (m_B == c.getBlue()) && (m_A == c.getAlpha()));
	}

	bool operator !=(const phColor& c) const
	{
		return ((m_R != c.getRed()) && (m_G != c.getGreen()) && (m_B != c.getBlue()) && (m_A != c.getAlpha()));
	}


private:
	unsigned short m_R;
	unsigned short m_G;
	unsigned short m_B;
	float 		m_A;
};

#endif