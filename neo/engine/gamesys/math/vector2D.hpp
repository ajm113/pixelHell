#ifndef VECTOR2D_HPP
#define VECTOR2D_HPP

#include <math.h>

class phVector2D
{
public:
	phVector2D()
	{
		m_X = 0;
		m_Y = 0;
	}

	phVector2D(const float x, const float y)
	{
		m_X = x;
		m_Y = y;
	}


	float getX() const{ return m_X; }
	float getY() const{ return m_Y; }

	void setX(float x){ m_X = x; }
	void setY(float y){ m_Y = y; }

	/* Operators */
	phVector2D& operator +=(const phVector2D& v)
	{
		m_X += v.getX();
		m_Y += v.getY();
		return (*this);
	}

	phVector2D& operator -=(const phVector2D& v)
	{
		m_X -= v.getX();
		m_Y -= v.getY();
		return (*this);
	}

	phVector2D& operator /=(const phVector2D& v)
	{
		m_X *= v.getX();
		m_Y *= v.getY();
		return (*this);
	}

	phVector2D& operator &=(const phVector2D& v)
	{
		m_X *= v.getX();
		m_Y *= v.getY();
		return (*this);
	}

	phVector2D& operator *=(float v)
	{
		m_X *= v;
		m_Y *= v;
		return (*this);
	}

	phVector2D& operator -=(float v)
	{
		m_X -= v;
		m_Y -= v;
		return (*this);
	}

	phVector2D& operator +=(float v)
	{
		m_X += v;
		m_Y += v;
		return (*this);
	}

	phVector2D operator -(void) const
	{
		return (phVector2D(-getX(), -getY()));
	}

	phVector2D operator *(float& x) const
	{
		return (phVector2D(m_X * x, m_Y * x));
	}

	phVector2D operator -(float& x) const
	{
		return (phVector2D(m_X - x, m_Y - x));
	}

	phVector2D operator +(const phVector2D& v) const
	{
		return (phVector2D(m_X + v.getX(), m_Y + v.getY()));
	}

	phVector2D operator -(const phVector2D& v) const
	{
		return (phVector2D(m_X - v.getX(), m_Y - v.getY()));
	}

	phVector2D operator *(const phVector2D& v) const
	{
		return (phVector2D(m_X * v.getX(), m_Y * v.getY()));
	}

	phVector2D operator &(const phVector2D& v) const
	{
		return (phVector2D(m_X * v.getX(), m_Y * v.getY()));
	}

	phVector2D operator /(float t) const
	{
		float f = 1.0 / t;
		return (phVector2D(m_X * f, m_Y * f));
	}

	bool operator ==(const phVector2D& v) const
	{
		return ((m_X == v.getX()) && (m_Y == v.getY()));
	}

	bool operator !=(const phVector2D& v) const
	{
		return ((m_X != v.getX()) && (m_Y != v.getY()));
	}

	phVector2D& operator /=(float v)
	{
		float f = 1.0F / v;
		m_X *= f;
		m_Y *= f;
		return (*this);
	}

	/* Helper functions */
        
	phVector2D& normalize(void)
	{
		return (*this /= sqrtf(m_X * m_X + m_Y + m_Y));
	}

	void rotate(float angle)
	{
		float s = sinf(angle);
		float c = cosf(angle);

		float nx = c * m_X - s * m_Y;
		float ny = s * m_X + c * m_Y;

		m_X = nx;
		m_Y = ny;
	}

	double dot(const phVector2D& v)
	{
		return getX() * v.getX() + getY() * v.getY();
	}

	float magnitude()
	{
		return (sqrtf((squareMagnitude())));
	}
	
	float inverseMagnitude()
	{
		return (1.0F / sqrtf(squareMagnitude()));
	}

	float squareMagnitude()
	{
		return (m_X * m_X + m_Y * m_Y);
	}

protected:
	float m_X;
	float m_Y;
	
};

#endif