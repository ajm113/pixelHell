#ifndef TIMER_HPP
#define TIMER_HPP

#include <SDL2/SDL.h>

class phTimer
{
    public:
		//Initializes variables
		phTimer();

		//The various clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		//Gets the timer's time
		unsigned int getTicks();

		//Checks the status of the timer
		bool isStarted();
		bool isPaused();

    private:
		//The clock time when the timer started
		unsigned int m_startTicks;

		//The ticks stored when the timer was paused
		unsigned int m_pausedTicks;

		//The timer status
		bool m_paused;
		bool m_started;

};

#endif