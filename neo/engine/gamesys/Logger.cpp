#include "Logger.hpp"

void phLogger::printError(const char* message, ...)
{
	/* Compile arguments into string. */
	char buffer[512];
	va_list arguments;
	va_start (arguments, message);
	vsprintf (buffer, message, arguments);
	va_end (arguments);
	print(phLogger::VERBOSE_LEVEL_ERROR, buffer);	
}

void phLogger::printWarning(const char* message, ...)
{
	/* Compile arguments into string. */
	char buffer[512];
	va_list arguments;
	va_start (arguments, message);
	vsprintf (buffer, message, arguments);
	va_end (arguments);
	print(phLogger::VERBOSE_LEVEL_WARNING, buffer);	
}

void phLogger::printMessage(const char* message, ...)
{
	/* Compile arguments into string. */
	char buffer[512];
	va_list arguments;
	va_start (arguments, message);
	vsprintf (buffer, message, arguments);
	va_end (arguments);
	print(phLogger::VERBOSE_LEVEL_MESSAGE, buffer);	
}

void phLogger::printDebug(const char* message, ...)
{
	/* Compile arguments into string. */
	char buffer[512];
	va_list arguments;
	va_start (arguments, message);
	vsprintf (buffer, message, arguments);
	va_end (arguments);
	print(phLogger::VERBOSE_LEVEL_DEBUG, buffer);	
}

void phLogger::print(phLogger::phVerboseLevel level, const char* message)
{
	/* print to everywhere possible. */
	printToConsole(message, level);
	printToLogFile(message, level);
}

void phLogger::printToConsole(const char* message, phLogger::phVerboseLevel level)
{
	if(level < m_verboseLevel) return;

	if(level >= VERBOSE_LEVEL_WARNING)
	{
		std::cerr << "[" << verboseLevelToString(level) << "]: " << message << std::endl;
		return;
	}

	std::cout << "[" << verboseLevelToString(level) << "]: " << message << std::endl;
}

void phLogger::printToLogFile(const char* message, phLogger::phVerboseLevel level)
{
	if(level < m_verboseLevelLogFile) return;

	m_logStream << "[" << verboseLevelToString(level) << "]: " << message << std::endl;
}

inline const char* phLogger::verboseLevelToString(phLogger::phVerboseLevel level)
{
	switch(level)
	{
		case VERBOSE_LEVEL_DEBUG:
			return "DEBUG";
		case VERBOSE_LEVEL_MESSAGE:
			return "MESSAGE";
		case VERBOSE_LEVEL_WARNING:
			return "WARNING";
		case VERBOSE_LEVEL_ERROR:
			return "ERROR";
	}

	return "UNKOWN";
}