#include "Event.hpp"
#include "EventDispatcher.hpp"


// pointer to singleton instance
phEventDispatcher* phEventDispatcher::_instanceOf = 0;

phEventDispatcher* phEventDispatcher::get() {
	if (_instanceOf)
		return _instanceOf;
	return _instanceOf = new phEventDispatcher();
}


void phEventDispatcher::registerHandler(phEventHandler *device) {
  device->setNextHandler(_deviceList);
  _deviceList = device;
}

void phEventDispatcher::sendEvent(phEvent e) {

  phEventHandler * curDevice = _deviceList;
  for (; curDevice; curDevice = curDevice->getNextHandler()) {
    assert(curDevice != curDevice->getNextHandler());
    curDevice->eventHandler(e);
  }
}