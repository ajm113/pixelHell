#include "gamesys/EngineCore.hpp"

phEngineCore* engine;

int main(int argc, char *argv[])
{
	/* Set for utf8 */
	setlocale(LC_CTYPE, "");
	engine = new phEngineCore();
	engine->setWindowTitle("Pixel Hell Engine 0.6.0");

	/* Do we need to use a mod? */
	if(argc == 2)
		engine->setModFolder(argv[1]);
	
	/* Initalize the game engine. */
	if(!engine->initialize())
		return 1;

	/* Run framework. */
	engine->run();

	/* Clean up memory when finished! */
	engine->clearAll();

	return 0;
}