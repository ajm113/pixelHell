#ifndef AI_HPP
#define AI_HPP

#include "../entities/Entity.hpp"


class phAI : protected phEntity
{
public:
	phAI();

	enum phAIState
	{
		S_IDLE      = 0,
		S_ATTACKING = 1,
		S_DEAD      = 3
	};

	void setState(phAIState x)    { m_state = x;    }
	void setHealth(int x)         { m_health = x;   }
	void setTeam(int x)           { m_team   = x;   }


	phAIState  getState()      {  return m_state;      }
	int        getHealth()     {  return m_health;     }
	int        getTeam()       {  return m_team;       }

	protected:
		unsigned int m_team;
		int          m_health;
		phAIState    m_state;
	private:
};


#endif