#include "PhysicsObject.hpp"

bool phPhysicsObject::collisionTest(phPhysicsObject* object, phVector2D & collisionNormal)
{
	switch(getCollisionModel()) {
		case phPhysicsObject::MODEL_SPHERE: {
			switch(object->getCollisionModel())
			{
				case phPhysicsObject::MODEL_SPHERE:
					return sphereToSphereTest(object);
				case phPhysicsObject::MODEL_AABB:
					return SphereToAABBTest(object);
				case phPhysicsObject::MODEL_NONE:
				default:
					return false;
			}
			break;
		}
		case phPhysicsObject::MODEL_AABB: {
			switch(object->getCollisionModel())
			{
				case phPhysicsObject::MODEL_SPHERE:
					return object->SphereToAABBTest(this);
				case phPhysicsObject::MODEL_AABB:
					return AABBToAABBTest(object);
				case phPhysicsObject::MODEL_NONE:
				default:
					return false;
			}
			break;		
		}

		case phPhysicsObject::MODEL_NONE: {
		default:
			return false;
		}
	}

	return false;
}

bool phPhysicsObject::sphereToSphereTest(phPhysicsObject* object)
{
	//Relative velocity.
	phVector2D dv = object->getVelocity() - getVelocity();

	//Relative position.
	phVector2D rp = object->getPosition() - getPosition();

	float r = getSize().getY() - object->getSize().getY();

	float pp = rp.squareMagnitude() - r*r;

	if( pp < 0 )
		return true;

	float pv = rp.squareMagnitude();

	if(pv >= 0)
		return false;

	//dV^2
	float vv = dv.squareMagnitude();
	//(3)Check if the spheres can intersect within 1 frame
	if ( (pv + vv) <= 0 && (vv + 2 * pv + pp) >= 0 )
		return false;

	//tmin = -dP*dV/dV*2 
	//the time when the distance between the spheres is minimal
	float tmin = -pv/vv;

	//Discriminant/(4*dV^2) = -(dp^2-r^2+dP*dV*tmin)
	return ( pp + pv * tmin > 0 );
}

bool phPhysicsObject::AABBToAABBTest(phPhysicsObject* object)
{
    if ( std::fabs(getPosition().getX() - object->getPosition().getX()) > (getSize().getX() + object->getPosition().getX()) ) return false;
    if ( std::fabs(getPosition().getY() - object->getPosition().getY()) > (getSize().getY() + object->getPosition().getY()) ) return false;
 
    // We have an overlap
    return true;
}

// True if the Sphere and AABB intersects
bool phPhysicsObject::SphereToAABBTest(phPhysicsObject* object)
{
	double squaredDistance = object->squaredDistPointAABB( getPosition() );

	// Intersection if the distance from center is larger than the radius
	// of the sphere.

	return squaredDistance <= (getSize().getY() * getSize().getY());
}

bool phPhysicsObject::isCollidingWithObject(phPhysicsObject* object)
{
	int objectCount = m_collidingList.size();

	for (int i = 0; i < objectCount; ++i)
	{
		if(m_collidingList.at(i) == object)
			return true;
	}

	return false;
}

bool phPhysicsObject::insertCollidingObject(phPhysicsObject* object)
{
	/* Do we already have this objecT? */
	if(isCollidingWithObject(object))
		return false;

	m_collidingList.push_back(object);
	return true;
}

bool phPhysicsObject::removeCollidingObject(phPhysicsObject* object)
{
	int objectCount = m_collidingList.size();
	for (int i = 0; i < objectCount; ++i)
	{
		if(m_collidingList.at(i) == object)
			m_collidingList.erase(m_collidingList.begin()+i);
			return true;
	}
	return true;
}

void phPhysicsObject::clearAll()
{
	m_collidingList.clear();
}

double phPhysicsObject::squaredDistPointAABB( phVector2D p)
{
	// Squared distance
	double squaredDistance = 0.0;

	phVector2D min, max;
	getAABBMinAndMax(min, max);

	squaredDistance += check( p.getX(), min.getX(), max.getX() );
	squaredDistance += check( p.getY(), min.getY(), max.getY() );

	return squaredDistance;
}

double phPhysicsObject::check ( const double pn, const double bmin, const double bmax ) {
        double out = 0;
        double v = pn;
 
        if ( v < bmin )
        {            
            double val = (bmin - v);            
            out += val * val;        
        }        
         
        if ( v > bmax )
        {
            double val = (v - bmax);
            out += val * val;
        }
 
        return out;
}