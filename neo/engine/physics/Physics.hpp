#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include "../gamesys/math/Vector2D.hpp"
#include "../gamesys/Class.hpp"
#include "../gamesys/Utils.hpp"
#include "../gamesys/Logger.hpp"
#include "PhysicsObject.hpp"
#include "../renderer/Renderer.hpp"

#include <vector>
#include <limits>
#include <stdlib.h>

class phPhysics : protected phClass, protected phRenderer
{
public:
	phPhysics(phLogger* logger) : 
	phRenderer(logger) {
		m_logger = logger;
		setDebugMode(false);
	}
	~phPhysics(){}

	bool render();

	void eventHandler(phEvent event);

	void step(float delta);

	bool addObject(phPhysicsObject* object);
	bool removeObject(phPhysicsObject* object);

	void clearAll();

	void setDebugMode(bool x){ m_debugMode = x;    }
	bool isDebugMode() const { return m_debugMode; }

	void setDeltaTime(float x) { m_deltaTime = x; }
	float getDeltaTime()  const { return m_deltaTime; }

	static const int MAX_SPEED = 100;

protected:
	void checkForCollisions(phPhysicsObject* object);

	inline float clamp(float x, float a, float b);

	std::vector<phPhysicsObject*> m_objects;

	void drawDebugBoundingBox(phPhysicsObject* object);

private:
	phLogger* m_logger;
	bool      m_debugMode;
	float     m_deltaTime;
};


#endif