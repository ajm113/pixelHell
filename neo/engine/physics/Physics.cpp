#include "Physics.hpp"


void phPhysics::eventHandler(phEvent event)
{

}

bool phPhysics::addObject(phPhysicsObject* object)
{
	if(!object)
		return false;

	m_objects.push_back(object);
	return true;
}

bool phPhysics::removeObject(phPhysicsObject* object)
{
	if(!object)
		return false;

	int objectCount = m_objects.size();

	for(int i = 0; i < objectCount; i++)
	{
		if(object == m_objects.at(i)) {

			SAFE_DELETE(m_objects.at(i));
			m_objects.erase(m_objects.begin()+i);
			return true;
		}
	}

	return false;
}


void phPhysics::clearAll()
{
	int objectCount = m_objects.size();

	for(int i = 0; i < objectCount; i++)
	{	
		SAFE_DELETE(m_objects.at(i));
	}

	m_objects.clear();
}

void phPhysics::step(float delta)
{
	int objectCount = m_objects.size();

	for (int i = 0; i < objectCount; ++i)
	{
		phPhysicsObject* obj = m_objects.at(i);
		/* Update positon of the object. */
		obj->update(delta);

		/* Can we put the object to sleep? */
		if(obj->getVelocity() == phVector2D())
		{
			obj->setSleep(true);
		}
		else
		{
			obj->setSleep(false);
		}

		if(!obj->isSleep() && !obj->isStatic())
		{
			checkForCollisions(obj);
		}
	}
}

void phPhysics::checkForCollisions(phPhysicsObject* object)
{
	int objectCount = m_objects.size();

	for (int i = 0; i < objectCount; ++i)
	{
		phPhysicsObject* obj = m_objects.at(i);

		if(object == obj)
			continue;

		phVector2D hitNormal;
		phVector2D velOut;

		if(object->collisionTest(obj, velOut)) {

			if(object->insertCollidingObject(obj)) {

				object->setVelocity(phVector2D(-object->getVelocity().getX(), -object->getVelocity().getY()));
				object->setAcceleration(phVector2D());

				object->onTouchStart(obj);
				m_logger->printDebug("Collision found between two objects! i: %i", i);
			}
		} else {
			if(object->removeCollidingObject(obj) ) {
				object->onTouchEnd(obj);
			}
		}
	}
}

void phPhysics::drawDebugBoundingBox(phPhysicsObject* object)
{
	/* Setup the color of the bounding box... */
	glColor3ub(255, 0, 0);

	if(object->isSleep())
		glColor3ub(0, 0, 255);


	switch(object->getCollisionModel())
	{
		case phPhysicsObject::MODEL_SPHERE: {
			drawBoundingSphere(object->getPosition(), object->getSize().getY());
			break;
		}
		case phPhysicsObject::MODEL_AABB: {
			phVector2D min, max;
			object->getAABBMinAndMax(min, max);
			drawBoundingBoxAABB(min, max);
			break;
		}
		case phPhysicsObject::MODEL_NONE: 
		default: {
			break;
		}
	}
}

bool phPhysics::render()
{
	if(!isDebugMode())
		return true; //No GL Errors.

	int objectCount = m_objects.size();

	for (int i = 0; i < objectCount; ++i)
	{
		phPhysicsObject* obj = m_objects.at(i);

		drawDebugBoundingBox(obj);
	}

	return checkGLErrors();
}