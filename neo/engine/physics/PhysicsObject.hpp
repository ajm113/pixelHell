#ifndef PHYSICS_OBJECT_HPP
#define PHYSICS_OBJECT_HPP

/*
	Base class for other phyiscal objects. i.e. static, physical, bullet.
*/

#include "../gamesys/math/vector2D.hpp"

#include <vector>
#include <cmath>

class phPhysicsObject
{
public:
	phPhysicsObject() 
	{
		setMaxSpeed(1000); 
		setSleep(false);
		setMass(0);
		setInverseMass(1); 
		setRestitution(0);
		setDamp(0.98f);
	}

	virtual ~phPhysicsObject() { clearAll(); }

	enum collisionModel {
		MODEL_NONE   = 0,
		MODEL_SPHERE = 1,
		MODEL_AABB   = 2
	};

	void setPosition(phVector2D x)     { m_position = x;     }
	void setDirection(phVector2D x)    { m_direction = x;    }
	void setVelocity(phVector2D x)     { m_velocity = x;     }
	void setAcceleration(phVector2D x) { m_acceleration = x; }
	void setSleep(bool x)              { m_sleep = x;        }
	void setSize(phVector2D x)         { m_size = x;         }
	void setMaxSpeed(float x)          { m_maxSpeed = x;     }
	void setMass(float x)          	{ m_mass = x;         }
	void setInverseMass(float x)       { m_inverseMass = x;  }
	void setRestitution(float x)       { m_restitution = x;  }
	void setCollidingObjects(std::vector<phPhysicsObject*> x)   {  m_collidingList = x; }
	void setCollisionModel(collisionModel x) { m_collsionModel = x; }
	void setDamp(float x)              { m_damp = x;         }

	phVector2D getPosition()     const {  return m_position;     }
	phVector2D getDirection()    const {  return m_direction;    }
	phVector2D getVelocity()     const {  return m_velocity;     }
	phVector2D getAcceleration() const {  return m_acceleration; }
	phVector2D getSize()         const {  return m_size;         }
	bool       isSleep()         const {  return m_sleep;        }
	float      getMaxSpeed()     const {  return m_maxSpeed;     }
	float      getMass()         const {  return m_mass;         }
	float      getInverseMass()  const {  return m_inverseMass;  }
	float      getRestitution()  const {  return m_restitution;  }
	float      getDamp()         const {  return m_damp;         }
	std::vector<phPhysicsObject*> getCollidingObjects() const { return m_collidingList; }
	collisionModel  getCollisionModel()  const {  return m_collsionModel;  }
	bool       isStatic() { return (getMass() < 0.0001f );  }

	void       getAABBMinAndMax(phVector2D & min, phVector2D & max)
	{
		//Bottom left.
 		min = phVector2D(-getSize().getX(), -getSize().getY());
 		//top left.
 		max = phVector2D(getSize().getX(), getSize().getY());
 
 		//Apply the position.
 		min += getPosition();
 		max += getPosition();
	}

	bool collisionTest(phPhysicsObject* object, phVector2D & collisionNormal);
	bool sphereToSphereTest(phPhysicsObject* object);
	bool AABBToAABBTest(phPhysicsObject* object);
	bool SphereToAABBTest(phPhysicsObject* object);

	bool isCollidingWithObject(phPhysicsObject* object);
	bool insertCollidingObject(phPhysicsObject* object);
	bool removeCollidingObject(phPhysicsObject* object);

	double squaredDistPointAABB( phVector2D p);

	void clearAll();

	/* Default math for our objects. */
	virtual void update(float delta){

		if(isStatic())
			return;

		setVelocity(getVelocity() + getAcceleration() * delta);
		setPosition(getPosition() + getVelocity() * delta);
	};

	virtual void onTouchStart(phPhysicsObject* obj){ return; }
	virtual void onTouchEnd(phPhysicsObject* obj){ return; }



protected:
	phVector2D m_position;
	phVector2D m_direction;
	phVector2D m_velocity;
	phVector2D m_acceleration;
	phVector2D m_size;
	bool       m_sleep;
	float      m_maxSpeed;
	float      m_mass;
	float      m_inverseMass;
	float      m_restitution;
	float      m_damp;
	collisionModel m_collsionModel;

	std::vector<phPhysicsObject*> m_collidingList;

private:

	double check( const double pn, const double bmin, const double bmax );
};


#endif