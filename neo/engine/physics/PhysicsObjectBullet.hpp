#ifndef PHYSICS_OBJECT_STATIC_HPP
#define PHYSICS_OBJECT_STATIC_HPP

#include "PhysicsObject.hpp"

class phPhysicsObjectBullet : protected phPhysicsObject
{
public:
	phPhysicsObjectBullet(){};
	virtual ~phPhysicsObjectBullet() {}
	
	void update(float delta);
};

#endif