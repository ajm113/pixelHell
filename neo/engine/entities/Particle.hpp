#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "Entity.hpp"

class phParticle : public phEntity{
public:

	phParticle(phLogger* logger)
	: phEntity(logger)
	{
	}

	phParticle()
	{
		m_lifespan = 0;
	}

	/* Update the position of the particle. */
	void update(float deltaTime)
	{
		setVelocity(getVelocity() + getAcceleration() * deltaTime);
		setPosition(getVelocity() + getPosition());
	}

	/* Draw the particle. */
	bool render()
	{

	}

	void setLifespan(float x) { m_lifespan = x; }

	float getLifespan() const { return m_lifespan;  }

	inline bool  isDead()
	{
		if(m_lifespan < 0.0)
			return true;
		return false;
	}

protected:

	float m_lifespan;

};

#endif