#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Entity.hpp"
#include "../gamesys/KeyMapper.hpp"

class phPlayer : public phEntity {
public:

	phPlayer(phLogger* logger, phKeyMapper* keyMapper)
	: phEntity(logger)
	{
		m_keyMapper = keyMapper;
	}

	void eventHandler(phEvent e);

	void updateLogic(float delta);

	enum phPlayerState {
		S_IDLE         = 1,
		S_MOVING       = 2,
		S_SHOOTING     = 3,
		S_ALT_SHOOTING = 4,
		S_DEAD         = 5
	};

	bool render();
	void setHealth(int x)         { m_health = x; }
	void setArmor(int x)          { m_armor = x;  }
	void setState(phPlayerState x){ m_state = x;  }

	int getHealth()          const {  return m_health;   }
	int getArmor()           const {  return m_armor;    }
	phPlayerState getState() const {  return m_state;    }

protected:
private:
	int           m_health;
	int           m_armor;
	phPlayerState m_state;
	phKeyMapper*  m_keyMapper;
};


#endif