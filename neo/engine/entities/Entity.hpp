#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "../gamesys/math/Vector2D.hpp"
#include "../gamesys/math/Color.hpp"
#include "../gamesys/Logger.hpp"
#include "../gamesys/Class.hpp"
#include "../renderer/Renderer.hpp"
#include "../physics/PhysicsObject.hpp"
#include "../gamesys/assetLoader/imageLoader/Image.hpp"

class phEntity : public phClass, public phRenderer, public phPhysicsObject
{
public:

	phEntity(phLogger* logger)
	: phRenderer(logger) {
		m_logger = logger;
	}

	virtual ~phEntity() {}

	virtual void eventHandler(phEvent event) = 0;
	virtual bool render() = 0;
	virtual void updateLogic(float delta) {}

	void     setColor(phColor x)    {  m_color = x;      }
	void     setImage(phImage* x)   {  m_image = x;      }

	phColor  getColor()      const  {  return m_color;   }
	phImage* getImage()      const  {  return m_image;   }

	protected:
		phColor   m_color;
		phImage*  m_image;
		phLogger* m_logger;
	private:
};


#endif