#ifndef BULLET_HPP
#define BULLET_HPP

#include "Entity.hpp"

class phBullet : public phEntity{
public:

	phBullet(phLogger* logger)
	: phEntity(logger)
	{
	}

	void setHitPoints(int x) { m_hitPoints = x; }
	void setSpeed(float x)   { m_speed = x;     }
	void setSize(float x)    { m_size = x;}

	int   getHitPoints() const { return m_hitPoints; }
	float getSpeed()     const { return m_speed;     }
	float getSize()      const { return m_size;      }

protected:
	int   m_hitPoints;
	float m_speed;
	float m_size;
};

#endif