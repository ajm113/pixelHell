#include "Player.hpp"

void phPlayer::eventHandler(phEvent event)
{

}

/* Render our player to the screen. */
bool phPlayer::render()
{
	enableTextures(getImage()->getGLId(), getImage()->hasAlpha());
	glColor3ub(m_color.getRed(), m_color.getGreen(), m_color.getBlue());
	drawSprite(getPosition(), getSize());
	disableTextures();
	return true;
}

void phPlayer::updateLogic(float delta)
{
	setAcceleration(phVector2D());
	phVector2D vVel = getAcceleration();

	if(m_keyMapper->isEventFired("+moveup"))
	{
		vVel.setY(1);
	}
	else if(m_keyMapper->isEventFired("+movedown"))
	{
		vVel.setY(-1);
	}
	
	if(m_keyMapper->isEventFired("+moveleft"))
	{
		vVel.setX(-1);
	}
	else if(m_keyMapper->isEventFired("+moveright"))
	{
		vVel.setX(1);
	}

	setAcceleration(vVel);
}