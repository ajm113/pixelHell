
#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include <string>

#include "Entity.hpp"
#include "Bullet.hpp"

class phWeapon : protected phEntity{
public:

	phWeapon(phLogger* logger)
	: phEntity(logger)
	{
	}

	void setHitPoints(int x)            { m_hitPoints = x;      }
	void setFireRate(float x)           { m_fireRate = x;       }
	void setSpreadMax(float x)          { m_spreadMax = x;      }
	void setSpreadMin(float x)          { m_spreadMin = x;      }
	void setName(const std::string & x) { m_name = x;           }
	void setBulletSpeedMin(float x)     { m_bulletSpeedMin = x; }
	void setBulletSpeedMax(float x)     { m_bulletSpeedMax = x; }

	int   getHitPoints()             const   { return m_hitPoints;      }
	float getFireRate()              const   { return m_fireRate;       }
	float getSpreadMax()             const   { return m_spreadMax;      }
	float getSpreadMin()             const   { return m_spreadMin;      }
	const std::string & getName()    const   { return m_name;           }
	float getBulletSpeedMin()        const   { return m_bulletSpeedMin; }
	float getBulletSpeedMax()        const   { return m_bulletSpeedMax; }

protected:

	std::string m_name;

	int    m_hitPoints;
	float  m_fireRate;
	float  m_spreadMax;
	float  m_spreadMin;
	float  m_bulletSpeedMin;
	float  m_bulletSpeedMax;

};

#endif