#ifndef PARTICLE_EMITTER_HPP
#define PARTICLE_EMITTER_HPP

#include "Entity.hpp"
#include "Particle.hpp"

class phParticleEmitter : public phEntity{
public:

	phParticleEmitter(phLogger* logger)
	: phEntity(logger)
	{
	}

	enum phEmitterType
	{
		E_LOOP   = 1,
		E_EFFECT = 2
	};

	static const int MAX_PARTICLES = 1000;

	phParticleEmitter()
	{
		m_type = E_LOOP;
		m_particles.resize(MAX_PARTICLES);
	}

	~phParticleEmitter()
	{
		m_particles.clear();
	}

	void update(float deltaTime);

	void setParticles(std::vector<phParticle> x) { m_particles = x;   }
	void setType(phEmitterType x)                { m_type = x;        }
	void setMaxLifespan(float x)                 { m_maxLifespan = x; }

	std::vector<phParticle> const getParticles()   { return m_particles;   }
	float                   const getMaxLifespan() { return m_maxLifespan; }
	phEmitterType           const getType()        { return m_type;        }

protected:

	std::vector<phParticle> m_particles;
	float                   m_maxLifespan;
	phEmitterType           m_type;
};

#endif